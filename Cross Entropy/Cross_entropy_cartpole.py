'''
Cross entropy on cartpole.  model is one hidden layerNN with ReLU and 128 hidden
nurons.Hyperparameter set randomly. will tune.
'''
import gym
from collections import namedtuple
import numpy as np
from tensorboardX import SummaryWriter

import torch 
import torch.nn as nn
import torch.optim as optim

HIDDEN_SIZE =128
BATCH_SIZE = 16
PERCENTILE= 70       #Reward boundary.

#now NN. takes a single observation from the Env. as input, output>a number for
#every action.include softmax nonlinearity at last layer, coz output of NN is 
#a Probability Distribution over actioons.
class Net(nn.Module):  #own modeled NN
    def __init__(self, obs_size, hidden_size, n_actions):
        super(Net,self).__init__()
        self.net = nn.Sequential(
                nn.Linear(obs_size, hidden_size),
                nn.ReLU(),
                nn.Linear(hidden_size, n_actions)
                )
        
    def forward(self, x):
        return self.net(x)
#Single episode storing    
Episode = namedtuple('Episode', field_names=['reward', 'steps',])
#episodestep, representiong one single step of agent in Env.
EpisodeStep = namedtuple('EpisodeStep', field_names=['observation', 'action'])

#Function that generates batches with episodes. taking Environment, then our
#NN network and count of episode on every iteration. and batch size accumulation. ALso, 
#declare the reward counter for current episode. then reset env.
#start the env. loop with while True: function. on every iteration converting the   
#current observation to a tensor.

def iterate_batches(env, net, batch_size):
    batch = []
    episode_reward = 0.0
    episode_steps = []
    obs = env.reset()
    sm = nn.Softmax(dim=1)
    while True:
        obs_v = torch.FloatTensor([obs])
        act_probs_v = sm(net(obs_v))
        act_probs = act_probs_v.data.numpy()[0] #convert track gradients to a numpy array
        action = np.random.choice(len(act_probs), p=act_probs) #obtaining actual action from random choice
        next_obs, reward, is_done, _ = env.step(action)
        episode_reward += reward            #getting next observation
        episode_steps.append(EpisodeStep(observation=obs, action=action))  #save the observation which used to choose action
        if is_done:  #handel situation, when current episode is over, when stick fall
            batch.append(Episode(reward=episode_reward, steps=episode_steps)) #append finalized episode to the batch
            episode_reward = 0.0  #saving reward
            episode_steps = []         #reset total reward accumulator
            next_obs = env.reset()    # reset env.
            if len(batch) == batch_size:   #when batch end, using yield the control is transfered to the outer iteration loop
                yield batch   
                batch = []  #and then continue next. python generator function,a 
                            #return statement terminates a function entirely, yield statement pauses the function saving 
                            #all its states and later continues from there on successive calls.
        obs = next_obs    #changing the observation

'''
This function is core of cross entropy.from the given batch of episodesand percentile values.
reward_bound calculates a boundary rewardwhich is used to filter elite episodes to train on
to obtain reward> using numpy percentile func. then calculating mean reward only for seeing.
then, will filter of our episodes at for > if loop section.here will check, for every episode
in the batch, check that the episode has a higher total reward then our boundary and if it 
has, we will count it to populate obs. and act. to train leter.
'''
def filter_batch(batch, percentile):
    rewards = list(map(lambda s: s.reward, batch))
    reward_bound = np.percentile(rewards, percentile)
    reward_mean = float(np.mean(rewards))

    train_obs = []
    train_act = []
    for example in batch:
        if example.reward < reward_bound:
            continue
        train_obs.extend(map(lambda step: step.observation, example.steps))
        train_act.extend(map(lambda step: step.action, example.steps))
    #Here, will convert obs. and actions from elite episode to tensor and return a touple
    #of 4. obs,act,bowndary of rwd, and mean rewd.
    train_obs_v = torch.FloatTensor(train_obs)
    train_act_v = torch.LongTensor(train_act)
    return train_obs_v, train_act_v, reward_bound, reward_mean

'''
Now, the main prt. first, creat all the required object.env., NN,tenbrd, 
then at foir loop, training loop, will iterate our batches, then performing filtering our
elite episodeusing filter_batch func.then taking act.reward bundry used for filtering and
for the mean reward.
then we zero gradients of network and pass observations to the net.obtaining its action scores.
the score are passed through theobjective func. which calc cross entropy btw the network
output and the action agent took.
'''

if __name__ == "__main__":
    env = gym.make("CartPole-v0")
    env = gym.wrappers.Monitor(env, directory="mon", force=True)   #for video
    obs_size = env.observation_space.shape[0]
    n_actions = env.action_space.n

    net = Net(obs_size, HIDDEN_SIZE, n_actions)
    objective = nn.CrossEntropyLoss()
    optimizer = optim.Adam(params=net.parameters(), lr=0.01)
    writer = SummaryWriter(comment="-cartpole")

    for iter_no, batch in enumerate(iterate_batches(env, net, BATCH_SIZE)):
        obs_v, acts_v, reward_b, reward_m = filter_batch(batch, PERCENTILE)
        optimizer.zero_grad()
        action_scores_v = net(obs_v)
        loss_v = objective(action_scores_v, acts_v)
        loss_v.backward()
        optimizer.step()
        print("%d: loss=%.3f, reward_mean=%.1f, reward_bound=%.1f" % (
            iter_no, loss_v.item(), reward_m, reward_b))
        writer.add_scalar("loss", loss_v.item(), iter_no)
        writer.add_scalar("reward_bound", reward_b, iter_no)
        writer.add_scalar("reward_mean", reward_m, iter_no)
        if reward_m > 199:  #why 199? in gym,cartpole is considered solve when mean rewd for last 100 epsd are grater then 195.
            print("Solved!")  #our model don't even need 100 stps/ or we could use TimeLimit waraper 200, to stop after 200 stps.
            break
    writer.close()


#the idea is to reinforce our net to carry out those elite actions which lead to good score.

