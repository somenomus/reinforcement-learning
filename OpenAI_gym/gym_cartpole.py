import gym

e = gym.make('CartPole-v0')
#we always need to reset the Env. after calling.
obs = e.reset()
#as we will see, observation is 4 numbers
print(obs)

#action space is discrite 2. means 0 or 1. 0>left shift, 1>right shift
print(e.action_space)

#this is Box 4, means a vector of size 4 with values inside[-inf, inf]
print(e.observation_space)
#that value is = 
print(e.step(0))
'''
Here, we execute action 0 and got tuple of 4 element. New observation, reward 1.0,
 done Flag=false, means episode is not over yet. and extra info abt invironment,
 which is empty.
 '''
 #now execute some sample command, and see the observation.Returning some random sample.
# execute by cell.
 
e.action_space.sample()
e.action_space.sample()
e.observation_space.sample()
e.observation_space.sample()

'''
NEW PROG. Random cartpole agent'''

import gym


if __name__ == "__main__":
    env = gym.make("CartPole-v0")

    total_reward = 0.0
    total_steps = 0
    obs = env.reset()

    while True:
        action = env.action_space.sample()
        obs, reward, done, _ = env.step(action)
        total_reward += reward
        total_steps += 1
        if done:
            break

    print("Episode done in %d steps, total reward %.2f" % (total_steps, total_reward))
    
#for cartpole, highest possible boundary step is 195. our is 25-30. poor.

#so, for improving this, Wrappers. The following code. N last observation

import gym

import random


class RandomActionWrapper(gym.ActionWrapper):
    def __init__(self, env, epsilon=0.1):            #epsilion> probablity of a random action replace
        super(RandomActionWrapper, self).__init__(env)
        self.epsilon = epsilon

    def action(self, action):
        if random.random() < self.epsilon:
            print("Random!")
            return self.env.action_space.sample()
        return action


if __name__ == "__main__":
    env = RandomActionWrapper(gym.make("CartPole-v0"))

    obs = env.reset()
    total_reward = 0.0

    while True:
        obs, reward, done, _ = env.step(0)  #issue same action,0
        total_reward += reward
        if done:
            break

    print("Reward got: %.2f" % total_reward)    
    
'''
Now, fun part, monitoring the cartpole.'''

import gym

if __name__ == "__main__":
    env = gym.make("CartPole-v0")
    env = gym.wrappers.Monitor(env, "recording" , force = True)
   

    total_reward = 0.0
    total_steps = 0
    obs = env.reset()

    while True:
        action = env.action_space.sample()
        obs, reward, done, _ = env.step(action)
        total_reward += reward
        total_steps += 1
        if done:
            break

    print("Episode done in %d steps, total reward %.2f" % (total_steps, total_reward))
    env.close()
    env.env.close()


